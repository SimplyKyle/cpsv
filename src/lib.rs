// use std::path::Path;
use std::process::Stdio;
use tokio::process::Command;

// use ffmpeg_next::media::Type as MediaType;
// use ffmpeg_next::format::input;
// use ffmpeg_next::Error::StreamNotFound;

pub fn is_string_numeric(str: &str) -> bool {
    for c in str.chars() {
        if !c.is_numeric() {
            return false;
        }
    }
    true
}

pub async fn get_duration(input_name: &str) -> Result<f64, std::num::ParseFloatError> {
    let f_probe = Command::new("ffprobe")
        .arg("-v")
        .arg("error")
        .arg("-show_entries")
        .arg("format=duration")
        .arg("-of")
        .arg("default=noprint_wrappers=1:nokey=1")
        .arg("-i")
        .arg(input_name)
        .output()
        .await
        .expect("ffprobe failed to get duration");
    let duration = String::from_utf8_lossy(&f_probe.stdout)
        .trim()
        .parse::<f64>();
    duration
}

pub async fn get_resolution(input_name: &str) -> (i32, i32) {
    let f_probe = Command::new("ffprobe")
        .arg("-v")
        .arg("error")
        .arg("-show_entries")
        .arg("stream=width,height")
        .arg("-of")
        .arg("default=noprint_wrappers=1:nokey=1")
        .arg("-i")
        .arg(input_name)
        .output()
        .await
        .expect("ffprobe failed to get resolution");

    let height: i32 = String::from_utf8(f_probe.stdout.clone())
        .expect("ffprobe failed to get height")
        .lines()
        .next_back()
        .unwrap()
        .parse()
        .expect("ffprobe failed to parse height");
    let width: i32 = String::from_utf8(f_probe.stdout)
        .expect("ffprobe failed to get width")
        .lines()
        .next()
        .unwrap()
        .parse()
        .expect("ffprobe failed to parse width");

    (height, width)
}

pub async fn get_audio_bitrate(input_name: &str) -> f64 {
    let f_probe = Command::new("ffprobe")
        .arg("-v")
        .arg("error")
        .arg("-select_streams")
        .arg("a:0")
        .arg("-of")
        .arg("default=noprint_wrappers=1:nokey=1")
        .arg("-show_entries")
        .arg("stream=bit_rate")
        .arg(input_name)
        .stdout(Stdio::piped())
        .output()
        .await
        .expect("ffprobe failed to get audio bitrate");
    // check if stdout is number, if not, use default value
    let audio_bitrate: f64 =
        if is_string_numeric(String::from_utf8(f_probe.stdout.clone()).unwrap().trim()) {
            String::from_utf8(f_probe.stdout)
                .expect("ffprobe failed to get audio bitrate")
                .trim()
                .parse::<f64>()
                .unwrap_or(0.0)
        } else {
            128000.0
        } / 1000.0;
    audio_bitrate
}

// pub fn frame_rate(source: &Path) -> (f64,f64) {
//     let ictx = input(&source).unwrap();
//     let input = ictx
//         .streams()
//         .best(MediaType::Video)
//         .ok_or(StreamNotFound).unwrap();
//     let rate = input.avg_frame_rate();
//     (f64::from(rate.numerator()),f64::from(rate.denominator()))
// }

use anyhow::{Context, Result};
use av1an_cli::{parse_cli, run, CliOpts};
use av1an_core::concat::ConcatMethod;
use av1an_core::context::Av1anContext;
use av1an_core::encoder::Encoder;
use av1an_core::hash_path;
use clap::Parser;
use std::fs;
use tokio::process::Command;

/// Search for a pattern in a file and display the lines that contain it.
#[derive(Parser)]
struct Cli {
    #[clap(short = 'a', long = "audio", help = "Leave blank to copy")]
    audio: Option<f64>,
    #[clap(short = 'u', long = "upload", help = "Upload the video to the server")]
    upload: bool,
    #[clap(short = 'e', long = "encoder", default_value = "aom", possible_values = &["aom", "rav1e", "vpx", "svt-av1"])]
    encoder: Encoder,
    // Input file
    #[clap(short = 'i', long = "input")]
    input: String,
    #[clap(short = 'c', long = "continue")]
    resume: bool,
    #[clap(short = 'k', long = "keep")]
    keep: bool,
    /// The path to the file to read
    #[clap(short = 'o', long = "output")]
    output: String,
    // Size in MiB
    #[clap(short = 's', long = "size")]
    size: Option<f64>,
    #[clap(short = 'f', long = "fps", help = "Leave blank to copy")]
    fps: Option<f64>,
    #[clap(short = 'r', long = "resolution", help = "Leave blank to copy")]
    resolution: Option<String>,
    #[clap(long = "super-compress")]
    super_compress: bool,
    #[clap(short = 'n')]
    only_calculate_size: bool,
    #[clap(long = "no-target-quality")]
    no_target_quality: bool,
    #[clap(long = "skip-startup-mkv-fix")]
    no_mkv_fix: bool,
    #[clap(long = "no-av1-psy-optimize")]
    no_optimize: bool,
    #[clap(long = "emergency-fix")]
    chunk_fix: bool,
    #[clap(long = "lookback", default_value = "64")]
    lookback: u32,
}
fn replace_params(name: &mut Vec<String>, search: &str, replace: Option<String>) {
    if let Some(x) = replace {
        let replace_position = name.iter().position(|x| x == search).unwrap() + 1;
        name[replace_position] = x;
    } else {
        let delete_position = name.iter().position(|x| x == search).unwrap() + 1;
        name.remove(delete_position - 1);
        name.remove(delete_position);
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Cli::parse();

    let org_path = std::env::current_dir()?;

    let (org_height, org_width): (i32, i32);
    let tmp_dir =
        std::path::PathBuf::from(format!(".{}-cpsv", hash_path(&org_path.join(&args.input))));

    if !tmp_dir.exists() {
        fs::create_dir_all(&tmp_dir)?;
    }

    let temp = format!("{}_mkvmerge_fix.mkv", &args.input);
    let mut broken_video = temp.as_str();

    if !args.no_mkv_fix {
        duct::cmd!("mkvmerge", &args.input, "-o", broken_video)
            .run()
            .context("mkvmerge failed")?;
        fs::rename(broken_video, tmp_dir.join(broken_video))?;
    } else {
        fs::copy(&args.input, tmp_dir.join(&args.input))?;
        broken_video = &args.input;
    }

    std::env::set_current_dir(&tmp_dir)?;

    let temp;

    (temp, (org_height, org_width)) = tokio::join!(
        cpsv::get_duration(broken_video),
        cpsv::get_resolution(broken_video)
    );
    let duration = temp.unwrap();

    let framerate_change_filename = format!("{}_framerate_change.mkv", &broken_video);
    let framerate_change_filepath = std::path::PathBuf::from(&framerate_change_filename);

    let org_bitrate = cpsv::get_audio_bitrate(broken_video).await;

    let input_fps = av1an_core::ffmpeg::frame_rate(&std::path::PathBuf::from(&broken_video))
        .context("failed to acquire frmerate from ffmpeg")?;
    dbg!(input_fps);

    let want_height;
    let mut want_fps;
    let audio_bitrate;
    if args.super_compress {
        // use fps=30, audio_bitrate=96, reso=720p when not specificed
        want_height = match args.resolution.as_ref() {
            None => 720.0_f64,
            Some(_) => args
                .resolution
                .as_ref()
                .unwrap() // impossible to fail
                .parse::<f64>()
                .context("Failed to parse resolution into float")?,
        };
        want_fps = args.fps.unwrap_or(30.0_f64);
        audio_bitrate = args.audio.unwrap_or(96.0);
    } else {
        // copy when not specified
        want_height = match args.resolution.as_ref() {
            None => org_height as f64,
            Some(_) => args
                .resolution
                .as_ref()
                .unwrap() // impossible to fail
                .parse::<f64>()
                .context("Failed to parse resolution into float")?,
        };
        want_fps = args.fps.unwrap_or(input_fps);
        audio_bitrate = args.audio.unwrap_or(org_bitrate);
    }
    // find the closest multiple of 30fps to the input fps
    let multiplier = input_fps.round() / want_fps.round();

    // `(a * 100.0).round() / 100.0`  means rounding a to 3 decimal places

    want_fps = (input_fps / multiplier * 1000.0).round() / 1000.0;

    if !args.only_calculate_size && !args.resume {
        let ffpb_args = [
            "-y",
            "-i",
            broken_video,
            "-preset",
            "ultrafast",
            "-qp",
            "0",
            "-c:v",
            "libx264",
            "-vf",
            &format!("fps=fps={want_fps},format=yuv420p10le,scale=-1:{want_height}"),
            // &format!("fps=fps={want_fps},format=yuv420p10le"),
            "-c:a",
            "libopus",
            "-b:a",
            &format!("{audio_bitrate}k"),
            &framerate_change_filename,
        ]
        .iter()
        .map(|&x| x.to_string())
        .collect::<Vec<String>>();
        ffpb::ffmpeg(&ffpb_args).with_context(|| {
            // str.extend(ffpb_args.iter().map(|x| format!("{} ", x)));
            let _ = Command::new("ffmpeg")
                .args(ffpb_args)
                .spawn()
                .context("this is expected to fail");
            "ffpb failed! running native ffmpeg to get proper error message"
        })?;
    }

    // calculate the bitrate of the video according to the size in MiB
    let bitrate: f64 = args.size.unwrap_or(0.0) * 8192.0 / duration - audio_bitrate;

    // round it because av1an only accepts integer bitrates
    let bitrate = bitrate.floor() as u32;
    let av1_output = format!("{}-av1an-out.mkv", &args.output);

    let bitrate: String = bitrate.to_string();

    if args.only_calculate_size {
        println!("{bitrate}");
    }

    let av1_output_file_path = std::path::PathBuf::from(&av1_output);

    let resume = args.resume;
    let keep = args.keep;
    let mut opts = CliOpts {
        input: vec![framerate_change_filepath.clone()],
        output_file: Some(av1_output_file_path),
        temp: None,
        quiet: false,
        verbose: false,
        chroma_noise: true,
        log_file: None,
        log_level: log::LevelFilter::Debug, // yes this is the default value
        resume,
        keep,
        force: false,
        overwrite: true,
        max_tries: 1, // easier debugging :)
        workers: 12,
        set_thread_affinity: None,
        scenes: None,
        split_method: av1an_core::SplitMethod::AvScenechange,
        sc_method: av1an_core::ScenecutMethod::Standard,
        sc_only: false,
        sc_pix_format: None,
        sc_downscale_height: None,
        extra_split: None,
        min_scene_len: 24,
        force_keyframes: None,
        encoder: args.encoder,
        video_params: None,
        passes: None,
        audio_params: None,
        ffmpeg_filter_args: None,
        chunk_method: None,
        chunk_order: av1an_core::ChunkOrdering::LongestFirst,
        photon_noise: Some(2),
        concat: ConcatMethod::MKVMerge,
        pix_format: ffmpeg::util::format::pixel::Pixel::YUV420P10LE,
        zones: None,
        vmaf: false,
        vmaf_path: None,
        vmaf_res: "1920x1080".to_string(),
        vmaf_threads: None,
        vmaf_filter: None,
        target_quality: if !args.no_target_quality {
            Some(95.0)
        } else {
            None
        },
        probes: 6,
        probing_rate: 1,
        probe_slow: false,
        min_q: None,
        max_q: None,
    };

    if args.encoder == Encoder::vpx || args.encoder == Encoder::svt_av1 {
        opts.photon_noise = None;
    }
    if args.chunk_fix {
        opts.chunk_method = Some(av1an_core::ChunkMethod::Select);
    }
    let encode_args = parse_cli(opts);
    for a in encode_args.unwrap() {
        let px = Av1anContext::new(a);
        let mut encode_args = px.unwrap().args;
        let mut addon_args = Vec::new();
        match args.encoder {
            Encoder::vpx | Encoder::aom => {
                if args.size.is_some() {
                    addon_args.push(format!("--target-bitrate={bitrate}"));
                }
                addon_args.push("--aq-mode=1".to_string());
                addon_args.push("--frame-parallel=1".to_string());
                addon_args.push(format!(
                    "--width={}",
                    (((org_width as f64) * (want_height / org_height as f64)).round() as i64)
                )); // 720p with the same aspect ratio as the input video
                addon_args.push(format!("--height={want_height}"));
                encode_args.video_params.iter_mut().for_each(|x| {
                    // replace automatic quality mode to constrained quality mode
                    if args.size.is_some() && !args.no_target_quality {
                        if x == "--end-usage=q" {
                            *x = "--end-usage=cq".to_string();
                        }
                    } else if args.size.is_some()
                        && args.no_target_quality
                        && (x == "--end-usage=q" || x == "--end-usage=cq")
                    {
                        *x = "--end-usage=vbr".to_string();
                    }
                    if x == "--cpu-used=6" {
                        *x = "--cpu-used=3".to_string();
                    }
                    if x == "--threads=8" {
                        *x = "--threads=4".to_string();
                    }
                });
                // unique fixes to aom and vpx
                match args.encoder {
                    Encoder::aom => {
                        addon_args.extend(
                            [
                                "--sb-size=dynamic",
                                "--enable-fwd-kf=1",
                                "--disable-kf",
                                "--deltaq-mode=0",
                                "--enable-dnl-denoising=0",
                                "--denoise-noise-level=5",
                                "--enable-qm=1",
                                // "--min-q=1",
                                "--sharpness=3",
                                "--enable-keyframe-filtering=2",
                                "--arnr-strength=1",
                                "--quant-b-adapt=1",
                                "--mv-cost-upd-freq=2",
                                "--enable-chroma-deltaq=0",
                                "--disable-trellis-quant=0",
                                "--enable-cdef=1",
                                "--enable-restoration=1",
                            ]
                            .map(|x| x.to_string())
                            .to_vec(),
                        );
                        if !args.no_optimize {
                            addon_args.extend(
                                ["--tune-content=psy", "--tune=ssim"]
                                    .map(|x| x.to_string())
                                    .to_vec(),
                            );
                            addon_args.push(format!("--lag-in-frames={}", args.lookback));
                        } else {
                            addon_args.push("--lag-in-frames=48".to_string());
                        }
                    }
                    Encoder::vpx => {
                        addon_args.push("--lag-in-frames=22".to_string());
                    }
                    _ => (),
                }
            }
            Encoder::rav1e => {
                replace_params(
                    &mut encode_args.video_params,
                    "--speed",
                    Some(4.to_string()),
                );
                addon_args.extend(["--threads", "2"].map(|x| x.to_string()).to_vec());
            }
            Encoder::svt_av1 => {
                addon_args.extend(
                    [
                        "--tune",
                        "0",
                        "--lookahead",
                        &args.lookback.to_string(),
                        "--enable-overlays",
                        "1",
                        "--scd",
                        "1",
                    ]
                    .map(|x| x.to_string())
                    .to_vec(),
                );
                replace_params(
                    &mut encode_args.video_params,
                    "--preset",
                    Some(3.to_string()),
                );
                if args.size.is_some() {
                    if args.no_target_quality {
                        replace_params(&mut encode_args.video_params, "--rc", Some(1.to_string()));
                        replace_params(&mut encode_args.video_params, "--crf", None);
                        addon_args
                            .extend(["--tbr", bitrate.as_str()].map(|x| x.to_string()).to_vec());
                    } else {
                        addon_args
                            .extend(["--mbr", bitrate.as_str()].map(|x| x.to_string()).to_vec());
                    }
                };
            }
            _ => unreachable!(), // restrained in clap
        }
        encode_args.video_params.append(&mut addon_args);
        // run av1an with my patch
        if !args.only_calculate_size {
            run(log::LevelFilter::Debug, vec![encode_args]).context("av1an bailing!")?;

            duct::cmd!(
                "mkvmerge",
                "-o",
                &args.output,
                &av1_output,
                "--default-duration",
                &format!("0:{}fps", want_fps)
            )
            .run()
            .context("mkvmerge merging failed")?;

            println!(
                "Your video file size is {} MiB, {} MB",
                fs::metadata(&args.output)?.len() / 1048576,
                fs::metadata(&args.output)?.len() / 1000000
            );

            fs::rename(&args.output, org_path.join(&args.output))?;

            // remove current directory
            let real_path = org_path.join(&tmp_dir);
            if !args.keep {
                fs::remove_dir_all(real_path)?;
            }
        }
    }
    Ok(())
}
